<?php
namespace ExchangeInterface;

interface ExchangeInterface {
    public function convert(float $euro);
}

?>