<?php
namespace Core\View;
class View implements \Core\View\ViewInterface 
{
    private $curencyName;
    const DEFAULT_TEMPLATE_FOLDER = 'views';
    const DEFAULT_TEMPLATE_EXTENSION = '.php';

    public function __construct($curencyName)
    {
        $this->curencyName = $curencyName;
    }

    public function render($value,$result,$euro)
    {
        include self::DEFAULT_TEMPLATE_FOLDER . DIRECTORY_SEPARATOR . 
                            $this->curencyName . self::DEFAULT_TEMPLATE_EXTENSION;
    }
}

