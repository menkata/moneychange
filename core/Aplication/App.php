<?php

namespace Core\Aplication;

class App 
{
    private $apiUrl;
    private $currency;
    public function __construct()
    {
        $this->setApiUrl();
    }
    
    private function setApiUrl()
    {
        $this->apiUrl = "https://api.exchangeratesapi.io/latest";
        $file = file_get_contents($this->apiUrl);
        $file2 = str_replace('{"rates":{' , '{"rates":{,' , $file);
        $this->currency = explode(',' , $file2);
    }

    public function getCurrency()
    {
        return $this->currency;
    }
}

?>