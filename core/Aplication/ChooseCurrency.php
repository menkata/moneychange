<?php

namespace Core\Aplication;

class ChooseCurrency
{
    private $curencyes;

    public function __construct(\Core\Aplication\App $curencyes)
    {
        $this->curencyes = $curencyes;
    }

    //function findCurency($curencyes){foreach $this->curencyes as value{if $this->curencyes == $curencyes...}}

    public function getBgn()
    {
        return $this->curencyes->getCurrency()[21];
    }

    public function getDollar()
    {
        return $this->curencyes->getCurrency()[27];
    }

    public function getGbp()
    {
        return $this->curencyes->getCurrency()[30];
    }

    public function getAllCurency()
    {
        return $this->curencyes->getCurrency();
    }

    public function getCurrentCurrency($key)
    {
        return $this->curencyes->getCurrency()[$key];
    }
}


?>