<?php
namespace CurrencyUnits\GBP;

class GBP implements \ExchangeInterface\ExchangeInterface
{
    private $fullValueName;
    private $curencyName;
    private $value;

    public function __construct(\Core\Aplication\ChooseCurrency $value)
    {
        // "GBP":0.89635
        $this->fullValueName = $value->getGbp();
        $gbp = explode(':',$this->fullValueName);
        $this->curencyName = str_replace('"', '', $gbp[0]);
        $this->value = round($gbp[1],2);
        //echo "today value of euro to GBP is (1 euro = $this->value GBP) -> " . $this->value . "<br />";

    }

    public function convert(float $euro)
    {
        $curencyName = $this->curencyName;
        $value = $this->value;
        $amount = $euro * $value;
        $result = round($amount,2);
        $view = new \Core\View\View($curencyName);
        $view->rendeR($value, $result, $euro);
    }

}

?>