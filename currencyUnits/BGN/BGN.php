<?php
namespace CurrencyUnits\BGN;

class BGN implements \ExchangeInterface\ExchangeInterface
{
    private $fullValueName;
    private $curencyName;
    private $value;

    public function __construct(\Core\Aplication\ChooseCurrency $value)
    {
        // "BGN":1.9558
        $this->fullValueName = $value->getBgn();
        $bgn = explode(':',$this->fullValueName);
        $this->curencyName = str_replace('"'  , '' ,$bgn[0]);
        $this->value = round($bgn[1],2);
        //echo "today value of euro to $this->curencyName is (1 euro = $this->value $this->curencyName) -> " . $this->value . "<br />";

    }

    public function convert(float $euro)
    {
        $curencyName = $this->curencyName;
        $value = $this->value;
        $amount = $euro * $value;
        $result = round($amount,2);
        $view = new \Core\View\View($curencyName);
        $view->rendeR($value, $result , $euro);
    }

}

?>