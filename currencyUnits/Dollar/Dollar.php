<?php
namespace CurrencyUnits\Dollar;

class Dollar implements \ExchangeInterface\ExchangeInterface
{
    private $fullValueName;
    private $curencyName;
    private $value;

    public function __construct(\Core\Aplication\ChooseCurrency $value)
    {
        // "USD":1.1058
        $this->fullValueName = $value->getDollar();
        $dollar = explode(':',$this->fullValueName);
        $this->curencyName = str_replace('"', '' , $dollar[0]);
        $this->value = round($dollar[1],2);
        //echo "today value of euro to dolar is (1 euro = $this->value dolars) -> " . $this->value . "<br />";

    }

    public function convert(float $euro)
    {
        $curencyName = $this->curencyName;
        $value = $this->value;
        $amount = $euro * $value;
        $result = round($amount,2);
        $view = new \Core\View\View($curencyName);
        $view->rendeR($value, $result, $euro);
    }

}

?>