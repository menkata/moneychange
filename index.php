<?php

spl_autoload_register();

use Core\Aplication\App;
use Core\Aplication\ChooseCurrency;
use Core\Aplication\ChooseMoneyCurency;


$app = new App();
$chooseCurency = new ChooseCurrency($app);

if(isset($_GET['convert']))
{
    if(isset($_GET['curency']))
    {
        $curency = $_GET['curency'];
        $myCurrency = new $curency($chooseCurency);
        /* $reflect = new \ReflectionClass($myCurrency);
        $getClassName = $reflect->getName();
        $ClassWithoutNamespace = explode('\\' , $getClassName); */
    
        if(isset($_GET['euro']))
        {
            $euro = round($_GET['euro'],2);
            if($euro == '')
            {
                throw new \Exception("First enter amount of euro you wanna change", 1);
            }

            $euro = $_GET['euro'];
            //$getConvertMethod = "convertEuroTo".$ClassWithoutNamespace[1];
            echo $myCurrency->convert($euro);
        }

    }

}

?>
<p> </p>

<form action="index.php" method="GET">
Please Choose your currency here : <select name="curency">
<option value='CurrencyUnits\BGN\BGN'> BGN </option>
<option value='CurrencyUnits\Dollar\Dollar'> Dolars </option>
<option value='CurrencyUnits\GBP\GBP'> GBP </option>
</select> <br />
Enter how much euro you want to change : <input type='text' name='euro' placeholder='sum in euro' pattern=".{1,7}"  required /> <br />
<input type='submit' name='convert' value='convert' />
</form>
